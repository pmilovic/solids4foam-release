#!/bin/bash

# Source tutorial run functions
. $WM_PROJECT_DIR/bin/tools/RunFunctions

# Source solids4Foam scripts
source solids4FoamScripts.sh

solids4Foam::caseOnlyRunsWithFoamExtend

if [ ! -f "$FOAM_USER_LIBBIN/libwaves2Foam.so" ]
then
    echo
    echo "libwaves2Foam.so is required for this tutorial, but it is not found"
    echo "in $FOAM_USER_LIBBIN/"
    echo
    exit 0
fi

# Create the mesh
runApplication blockMesh

# Compute the wave parameters
runApplication setWaveParameters

# Set the wave field
runApplication setWaveField

# Run the solver
if [[ "$1" == "parallel" ]]; then
    # Issue with waves2Foam boundary conditions looking for waveProperties when
    # libwaves2Foam.so is loaded. A workaround is to temporarily comment it and
    # then uncomment it to run the solver
    \sed -i 's|libs            ("libwaves2Foam.so");|//libs            ("libwaves2Foam.so");|g' system/controlDict
    runApplication decomposePar
    \sed -i 's|//libs            ("libwaves2Foam.so");|libs            ("libwaves2Foam.so");|g' system/controlDict
    runParallel solids4Foam 2
    runApplication reconstructPar
else
    runApplication solids4Foam
fi

# ----------------------------------------------------------------- end-of-file

